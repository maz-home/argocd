# ArgoCD

## ArgoCD Deployments in my kubernetes cluster with argocd 

```bash
kubectl apply -k .
```

## Get agorCD Admin password

```
kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2
```
